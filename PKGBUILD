# Maintainer: Bernhard Landauer <bernhard@manjaro.org>

pkgname=extra-cmake-modules
pkgver=5.245.0.r3645.g897fd6cb
pkgrel=1
pkgdesc='Extra modules and scripts for CMake'
arch=(x86_64 aarch64)
url='https://community.kde.org/Frameworks'
license=(LGPL)
replaces=($pkgname-git)
depends=(cmake)
makedepends=(git python-sphinx python-requests qt6-tools qt6-declarative)
optdepends=('python-pyxdg: to generate fastlane metadata for Android apps'
            'python-requests: to generate fastlane metadata for Android apps'
            'python-yaml: to generate fastlane metadata for Android apps')
groups=(kf6)
source=("git+https://github.com/KDE/$pkgname.git"
        ECM-no-init.py.patch
        enable_Bsymbolic-functions.patch)
sha256sums=('SKIP'
            '5695e45c7621a00c0bca28f058c13b5d524f963a00b53337c8cefcdaf22c4b52'
            '34d9ff7de4335704be018fad0b3bb477ee40a4b9d56ec72610d3a1fed21e4f01')

pkgver() {
  cd $pkgname
  _ver=5.245.0
  echo "${_ver}.r$(git rev-list --count HEAD).g$(git rev-parse --short HEAD)"
}

prepare() {
  patch -d $pkgname -p1 -i ../ECM-no-init.py.patch # Don't create __init__.py
  patch -d $pkgname -p1 -i ../enable_Bsymbolic-functions.patch
}

build() {
  cmake -B build -S $pkgname \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DBUILD_HTML_DOCS=OFF \
    -DBUILD_QTHELP_DOCS=ON
  cmake --build build
}

package() {
  DESTDIR="$pkgdir" cmake --install build
}
